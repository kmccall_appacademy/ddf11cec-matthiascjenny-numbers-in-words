require 'byebug'

class Fixnum

  def in_words
    arr = split_up(self)
    result = ""
    if arr == ["0"]
      return "zero"
    else
      if arr.length == 5
        result << to_999(arr[0]) unless arr[0] == "000"
        result << " trillion " unless arr[0] == "000"
        arr.shift
      end
      if arr.length == 4
        result << to_999(arr[0]) unless arr[0] == "000"
        result << " billion " unless arr[0] == "000"
        arr.shift
      end
      if arr.length == 3
        result << to_999(arr[0]) unless arr[0] == "000"
        result << " million " unless arr[0] == "000"
        arr.shift
      end
      if arr.length == 2
        result << to_999(arr[0]) unless arr[0] == "000"
        result << " thousand " unless arr[0] == "000"
        arr.shift
      end
      result << to_999(arr[0])
    end
    if result[0] == " "
      result = remove_first(result)
    end
    if result[-1] == " "
      arr = result.split("")
      arr.pop
      result = arr.join("")
    end
    arr = result.split("")
    i = 0
    while i + 1 < arr.length
      if arr[i] == " " && arr[i+1] == " "
        arr.delete_at(i)
        i += 1
      else
        i += 1
      end
    end
    return arr.join("")
  end

  def split_up(n)
    arr = []
    i = -3
    while i.abs <= n.to_s.length
      arr.unshift(n.to_s[i,3])
      i -= 3
    end
    if n.to_s.length % 3 == 2
      arr.unshift(n.to_s[0,2])
    elsif
      n.to_s.length % 3 == 1
        arr.unshift(n.to_s[0])
    end
    return arr
  end

  def singles(n)
    if n == "0"
      return ""
    elsif n == "1"
      return "one"
    elsif n == "2"
      return "two"
    elsif n == "3"
      return "three"
    elsif n == "4"
      return "four"
    elsif n == "5"
      return "five"
    elsif n == "6"
      return "six"
    elsif n == "7"
      return "seven"
    elsif n == "8"
      return "eight"
    else
      return "nine"
    end
  end

  def remove_first(s)
    arr = s.split("")
    arr.shift
    return arr.join("")
  end

  def to_999(n)
    result = ""
#    debugger
    if n.length == 3
      if n[0] == "0"
        n = remove_first(n)
      else
        result << singles(n[0]) << " hundred"
        n = remove_first(n)
      end
    end
    if n.length == 2 && n[0] != "1"
      if n[0] == "0"
        n = remove_first(n)
      elsif n[0] == "9"
        result << " ninety"
        n = remove_first(n)
      elsif n[0] == "8"
        result << " eighty"
        n = remove_first(n)
      elsif n[0] == "7"
        result << " seventy"
        n = remove_first(n)
      elsif n[0] == "6"
        result << " sixty"
        n = remove_first(n)
      elsif n[0] == "5"
        result << " fifty"
        n = remove_first(n)
      elsif n[0] == "4"
        result << " forty"
        n = remove_first(n)
      elsif n[0] == "3"
        result << " thirty"
        n = remove_first(n)
      elsif n[0] == "2"
        result << " twenty"
        n = remove_first(n)
      end
    end
    if n.length == 2 && n[0] == "1"
      if n == "19"
        result << " nineteen"
      elsif n == "18"
        result << " eighteen"
      elsif n == "17"
        result << " seventeen"
      elsif n == "16"
        result << " sixteen"
      elsif n == "15"
        result << " fifteen"
      elsif n == "14"
        result << " fourteen"
      elsif n == "13"
        result << " thirteen"
      elsif n == "12"
        result << " twelve"
      elsif n == "11"
        result << " eleven"
      elsif n == "10"
        result << " ten"
      end
    end
    result << " " << singles(n[-1]) unless n[-1] == "0" || n[-2] == "1"
    return result
  end

end
